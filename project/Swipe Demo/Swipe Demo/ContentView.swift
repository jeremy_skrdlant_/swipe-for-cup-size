//
//  ContentView.swift
//  Swipe Demo
//
//  Created by Jeremy Skrdlant on 10/4/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image("CaseyCups").resizable().aspectRatio(contentMode: .fit)
                .frame(width: nil, height: 250, alignment: .center)
            Spacer()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
