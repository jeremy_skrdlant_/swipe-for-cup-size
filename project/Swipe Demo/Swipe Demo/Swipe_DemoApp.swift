//
//  Swipe_DemoApp.swift
//  Swipe Demo
//
//  Created by Jeremy Skrdlant on 10/4/20.
//

import SwiftUI

@main
struct Swipe_DemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
